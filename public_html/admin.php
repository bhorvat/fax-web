<?php

if (session_status() == PHP_SESSION_NONE) {
    session_start();


    
  }
  if (!isset($_SESSION['username'])) {
    header('location: index.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: index.php");
  }
  $name = "";
  $quantity = "";
    $box="";
  $day = "";
  $month = "";
  $year = "";
  $hour = "";
  $minute = "";
  $second = "";
  $check = "";
  $errors = array();
  $db = mysqli_connect('localhost:3306', 'pillorg1_pill', 'k5j5z5f4q0', 'pillorg1_pill');
$user=($_SESSION['username']);

  
  //$query = "SELECT * FROM pill";
   $query = "SELECT * FROM pill ORDER BY datetime ASC";

  $results = mysqli_query($db, $query);
  $row = mysqli_fetch_assoc($results);
  $date = date("Y");

  if (isset($_POST['add_item'])) {
   
  
    $name = mysqli_real_escape_string($db, $_POST['name']);
    $quantity = mysqli_real_escape_string($db, $_POST['quantity']);
      $box=mysqli_real_escape_string($db, $_POST['box']);
    $day = mysqli_real_escape_string($db, $_POST['day']);
    $month = mysqli_real_escape_string($db, $_POST['month']);
    $year = mysqli_real_escape_string($db, $_POST['year']);
    $hour = mysqli_real_escape_string($db, $_POST['hour']);
    $minute = mysqli_real_escape_string($db, $_POST['minute']);
    $second = mysqli_real_escape_string($db, $_POST['second']);
    $check = mysqli_real_escape_string($db, $_POST['check']);
  
  
   if (empty($day) && empty($month) && empty($year) && empty($hour) && empty($minute) && empty($second)) {
      array_push($errors, "Atleast one variable must be filled!");
  }
  
     $box_check_query="SELECT * FROM pill WHERE box='$box'";
   $result_box=mysqli_query($db, $box_check_query);
   $box_postoji=mysqli_fetch_assoc($result_box);

   if($box_postoji){
       array_push($errors,"There is already medicine assigned to that box!");

   }


  
  
    if ( isset($_POST['check']) ) {
    $check = "on";
    } else { 
    $check = "off";
    }
if (count($errors) == 0) {
    $datetime= date(DATE_ATOM,mktime($hour,$minute,$second,$month,$day,$year));
     //$query = "INSERT INTO pill (name, quantity, box, day, month, year, hour, minute, second, check_value) VALUES ('$name', '$quantity','$box', '$day', '$month', '$year', '$hour', '$minute', '$second', '$check')";
      $query = "INSERT INTO pill (name, quantity, box, day, month, year, hour, minute, second, check_value, datetime) VALUES ('$name', '$quantity','$box', '$day', '$month', '$year', '$hour', '$minute', '$second', '$check','$datetime')";

      mysqli_query($db, $query);
      header('location: admin.php');
}
  }
  if (isset($_POST['new_table'])) {
   
      $query = "DELETE FROM pill WHERE id != 1";
      mysqli_query($db, $query);
      header('location: admin.php');
   
  }


?>
<!DOCTYPE html>
<html lang="en">
<head><meta charset="shift_jis">
    
   <meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=1">
   <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style_admin.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <title>Pill Organiser</title>
</head>

<body onload="chkuser()">
    <h1 id="title">Pill Organiser By Fusion Team</h1>
    <hr>
    <div class="container" id="buttons">
    <a id = "lg" class="btn" href="admin.php?logout='1'"><span class="glyphicon glyphicon-log-in"></span> Logout</a>
    <div class="row">
    <div class="column">
    <button class ="btn" id="show" onclick="show_feed()">Show Table
    
    <svg class="bi bi-calendar-check-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M4 .5a.5.5 0 0 0-1 0V1H2a2 2 0 0 0-2 2v1h16V3a2 2 0 0 0-2-2h-1V.5a.5.5 0 0 0-1 0V1H4V.5zM0 5h16v9a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V5zm10.854 3.854a.5.5 0 0 0-.708-.708L7.5 10.793 6.354 9.646a.5.5 0 1 0-.708.708l1.5 1.5a.5.5 0 0 0 .708 0l3-3z"/>
</svg>
    </button>
   
    <button class ="btn" id ="add" onclick="show_add_new()">Add New Alarm   					
    <svg class="bi bi-alarm-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M5.5.5A.5.5 0 0 1 6 0h4a.5.5 0 0 1 0 1H9v1.07a7.002 7.002 0 0 1 3.537 12.26l.817.816a.5.5 0 0 1-.708.708l-.924-.925A6.967 6.967 0 0 1 8 16a6.967 6.967 0 0 1-3.722-1.07l-.924.924a.5.5 0 0 1-.708-.708l.817-.816A7.002 7.002 0 0 1 7 2.07V1H5.999a.5.5 0 0 1-.5-.5zM.86 5.387A2.5 2.5 0 1 1 4.387 1.86 8.035 8.035 0 0 0 .86 5.387zM13.5 1c-.753 0-1.429.333-1.887.86a8.035 8.035 0 0 1 3.527 3.527A2.5 2.5 0 0 0 13.5 1zm-5 4a.5.5 0 0 0-1 0v3.882l-1.447 2.894a.5.5 0 1 0 .894.448l1.5-3A.5.5 0 0 0 8.5 9V5z"/>
</svg></button>

    <form class ="btn" method="post" onsubmit="return confirm('Are you sure you want to delete current table? This procces is irreversible!');">
    <button class ="btn" name="new_table" id="delete" type="submit" >Delete Table
    
    <svg class="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
</svg>
    </button>
    
    </form>
    </div>
  </div>
</div>
<div class="footer" id="footer">
  <p>Copyright © 2020 FusionTeam All rights reserved. </p>
</div>

<div id="feed" class="container">
<?php
 include('errors.php');

  echo "
  <div id='white_div' style='padding:15px;border-radius: 15px;'>
  <table  class='w3-table-all w3-hoverable' id='myTable'>
  <tr>
   <th>Pill name</th>
  <th>Quantity</th>
    <th>Box</th>
   <th>Time (h:m:s)</th>
  <th>Date (d.m.y)</th>
  <th>Repeat</th>
  <th></th>
</tr>
  ";
  
  while($row = mysqli_fetch_assoc($results))
  {
       $mydateformatVrijeme =date("H:i:s",mktime($row['hour'],$row['minute'],$row['second']));
      $mydateformatDatum=$row['day'].".".$row['month'].".".$row['year'].".";

  echo"

  <tr>
    <td>" . $row['name'] .  "</td>
    <td>" . $row['quantity'] .  "</td>
     <td>" . $row['box'] .  "</td>
    <td>" . $mydateformatVrijeme .  "</td>
    <td>" . $mydateformatDatum .  "</td>
    <td>" . $row['check_value'] .  "</td>
    <td><a class='btn' id='remove_btn' href=delete.php?id=".$row['id'].">Remove</a></td>
  </tr>
  ";
}
echo"</table>

</div>";


echo "<div id='user_txt'>Current user ",$user;

if ($user=='admin' ||$user=='LumenEngineering'){echo " has unrestricted  access level!";}
else{echo " has restricted  access level!";}
 echo"</div>"  ;

 

?>
</div>



<div id="add_new" class="container">
    <div style='background-color:white;padding:15px;border-radius: 15px;'>
<h2>Add new alarm in 24h format</h2>
  <hr>
  <form  name="myForm" method="post" onsubmit="return validateFormLogin()" action="admin.php">
    <?php include('errors.php'); ?>
      <div class="form-group">
          
          

          
          
  	  <label>Pill name</label>
  	  <input  placeholder="Enter pill name" pattern="[A-Za-z0-9]+" title="A-Z a-z 0-9" type="text" name="name"  required="required"  minlength="3" maxlength="20" id ="input_name" class="form-control" value="<?php echo $name; ?>">
  	</div>
      <div class="form-group">
  	  <label>Quantity</label>
  	  <input  placeholder="Enter pill quantity" type="number   required="required" name="quantity" min="1" max="30" id ="input_quantity" class="form-control" value="<?php echo $quantity; ?>">
  	</div>
  	    <div class="form-group">
        <label>Box</label>
        <input placeholder="Enter pill box" type="number" required="required" name="box" min="1" max="28" id ="input_box" class="form-control" value="<?php echo $box; ?>">
    </div>
      <table id='table2'>
          <tr>
              <td id="td1">

                  <div class="form-group">
                      <label>Day</label>
                      <input placeholder="Day" type="number" name="day" id ="input_day" class="form-control" " min="1" max="31" value="<?php echo $day; ?>">
                  </div>
              </td>
              <td id="td2">
                  <div class="form-group">
                      <label>Month</label>
                      <input placeholder="Month" type="number" name="month" id ="input_month" class="form-control" min="1" max="12" value="<?php echo $month; ?>">
                  </div>
              </td>
              <td id="td3">
                  <div class="form-group">
                      <label>Year</label>
                      <input placeholder="Year" type="number" name="year" id ="input_year" id ="input_year" class="form-control" min="2020" value="<?php echo $year; ?>">
                  </div>
              </td>
              </tr>
          <tr>
              <td id="td4">
                  <div class="form-group">
                      <label>Hour</label>
                      <input placeholder="Hours" type="number" name="hour" id ="input_hour" class="form-control" min="0" max="24" value="<?php echo $hour; ?>">
                  </div>
              </th>
              <td id="td5">
                  <div class="form-group">
                      <label>Minute</label>
                      <input placeholder="Minutes" type="number" name="minute" id ="input_minute" class="form-control" min="0" max="59" value="<?php echo $minute; ?>">
                  </div>
              </td>
              <td id="td6">
                  <div class="form-group">
                      <label>Second</label>
                      <input placeholder="Seconds" type="number" name="second" id ="input_second" class="form-control" min="0" max="59" value="<?php echo $second; ?>">
                  </div>
              </td>
          </tr>

      </table>
        <div class="form-group">
        <input type="checkbox" onclick="myFunction()" name="check" id="check_value">
        <label for="check_value">Repeat</label><br>
        </div>
    <div class="form-group">
  	  
  	  <button type="submit" class="btn" id="add_db" name="add_item">Insert <svg class="bi bi-server" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
  <path d="M13 2c0-1.105-2.239-2-5-2S3 .895 3 2s2.239 2 5 2 5-.895 5-2z"/>
  <path d="M13 3.75c-.322.24-.698.435-1.093.593C10.857 4.763 9.475 5 8 5s-2.857-.237-3.907-.657A4.881 4.881 0 0 1 3 3.75V6c0 1.105 2.239 2 5 2s5-.895 5-2V3.75z"/>
  <path d="M13 7.75c-.322.24-.698.435-1.093.593C10.857 8.763 9.475 9 8 9s-2.857-.237-3.907-.657A4.881 4.881 0 0 1 3 7.75V10c0 1.105 2.239 2 5 2s5-.895 5-2V7.75z"/>
  <path d="M13 11.75c-.322.24-.698.435-1.093.593-1.05.42-2.432.657-3.907.657s-2.857-.237-3.907-.657A4.883 4.883 0 0 1 3 11.75V14c0 1.105 2.239 2 5 2s5-.895 5-2v-2.25z"/>
</svg></button>
  	  
  	 
  	
  	

  	<div id="feed" class="container">
<?php
echo "<label id='td4'>Red or A indicates a box with asigned medicine and green or F indicates free box. </label>";

  echo "
  <div id='white_div' style='padding:15px;border-radius: 15px;'>
  <table  class='w3-table-all w3-hoverable' id='myTable'>
  	 

  	    
<tr>
  ";
  
for($j_box=1;$j_box<=28;$j_box++){
   $box_check_query="SELECT * FROM pill WHERE box='$j_box'";
   $result_box=mysqli_query($db, $box_check_query);
   $box_postoji=mysqli_fetch_assoc($result_box);


   if($box_postoji){
       echo"<th id='postoji'>";
      echo $j_box."-A";
      
   }
      if(!$box_postoji){
          echo"<th id='nepostoji'>";
    //  echo        "nepostoji".$jbox." "   ;
       echo       $j_box."-F";
   }

    
  echo"</th>"; 
  if($j_box%7==0){echo"</tr>";}
}
    echo"</table>";
    

?>
  	</div>
  	 
  	  

  </form>
  </div>

  
</div>
    	        


<script>
function show_feed() {

var x = document.getElementById("feed");
var y = document.getElementById("add_new");

  x.style.display = "block";
  y.style.display = "none";

  window.scrollBy(0, -1000);
}
function show_add_new() {

var x = document.getElementById("add_new");
var y = document.getElementById("feed");

  x.style.display = "block";
  y.style.display = "none";

  window.scrollBy(0, -1000);
}

function chkuser(){
    var user = <?php echo(json_encode($user)); ?>;
    
       
          
     if(user=='admin' || user=='LumenEngineering'){
         
     }
      else{   
        document.getElementById("delete").disabled  = true; 
       //  document.getElementById("remove_btn").disabled  = false; 
     //   document.getElementById("remove_btn").style.visibility = "hidden"; 
         // alert("Delete table feature is disabled for user "+user);
         //|| user!="LumenEngineering"
     }
     
     

     
        
         
     //.style.visibility = 'hidden';
    
    
    
}

function myFunction() {
  var checkBox = document.getElementById("check_value");
  
  if (checkBox.checked == true){
   
   document.getElementById("input_day").readOnly = true;
   document.getElementById("input_month").readOnly = true;
   document.getElementById("input_year").readOnly = true;
   document.getElementById("input_hour").readOnly = true;
   document.getElementById("input_minute").readOnly = true;
   document.getElementById("input_second").readOnly = true;
   
  }else{
 
  document.getElementById("input_day").readOnly = false;
  document.getElementById("input_month").readOnly = false;
  document.getElementById("input_year").readOnly = false;
  document.getElementById("input_hour").readOnly = false;
  document.getElementById("input_minute").readOnly = false;
  document.getElementById("input_second").readOnly = false;
  
  }
}
</script>


</body>
</html>
