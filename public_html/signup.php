
<?php


?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pill Organiser</title>
	
	<?php
	if(isset($_GET['error'])){
	  
	   if($_GET['error'] == 'passwordMatch'){
	       echo '<h5 style="color:red;text-align:center;">Passwords dont match!</h5>';
	   }
	    
	}
	
	
	?>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

 <form class="form-signup" action="register.php" method="post">
	<div class="container-contact100">

		<div class="wrap-contact100">
			<form method="post" class="contact100-form validate-form">
			
				<span class="contact100-form-title">
					Pill Organiser Registration
				</span>
 <h6 style="color: SlateBlue; padding-bottom:10px;">Enter your new username:</h6>
				<div class="wrap-input100 validate-input" data-validate="Pick your username">
				   
					<input class="input100" pattern="[A-Za-z0-9]+.{3,10}" title="A-Z a-z 0-9 min:3 max 10" type="text" name="uid" placeholder="Username" maxlength="10">
					<span class="focus-input100"></span>
				</div>
				
				 <h6 style="color: SlateBlue; padding-bottom:10px;">Pick your email adress:</h6>
								<div class="wrap-input100 validate-input" data-validate="Enter email">
					<input class="input100" pattern="^[^@]+@[^@]+\.[^@]+$" title="email" type="text" name="mail" maxlength="50" placeholder="Pick email">
					<span class="focus-input100"></span>
				</div>
									
<h6 style="color: SlateBlue; padding-bottom:10px;">Pick your password adress:</h6>

				<div class="wrap-input100 validate-input" data-validate = "Please pick your password">
					<input class="input100" type="password" name="pwd" placeholder="Password" maxlength="50">
					<span class="focus-input100"></span>
				</div>
				
				<h6 style="color: SlateBlue; padding-bottom:10px;">Repeat your password adress:</h6>
								<div class="wrap-input100 validate-input" data-validate = "Please repeat your password">
					<input class="input100" type="password" name="pwd_rpt" placeholder="Repeat password" maxlength="50">
					<span class="focus-input100"></span>
				</div>

				
				<div class="container-contact100-form-btn">
					<button  type="submit" name="signup-submit" class="contact100-form-btn">
						<span>
							<i class="fa fa-paper-plane-o m-r-6" aria-hidden="true"></i>
							Register
						</span>
					</button>
				</div>
	  </form>
	<h5 style="text-align:center;">or</h5>
	<form action="index.php">
							<div class="container-contact100-form-btn">	
		 <button type="submit" class="btn btn-primary mb-2">Back to Login</button>
		 </div>
		 </form>
			
							<div class="footer" id="footer">
  <p style="margin: auto; width: 100%; padding-top:50px;">Copyright © 2020 FusionTeam All rights reserved.</p>
</div>
			
		</div>
		

		
	</div>



	<div id="dropDownSelect1"></div>

	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/main.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>
