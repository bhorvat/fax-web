-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 02, 2020 at 01:12 AM
-- Server version: 5.7.30-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pillorga_pill`
--

-- --------------------------------------------------------

--
-- Table structure for table `pill`
--

CREATE TABLE `pill` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(5) NOT NULL,
  `box` int(10) NOT NULL,
  `day` int(10) NOT NULL,
  `month` int(10) NOT NULL,
  `year` int(10) NOT NULL,
  `hour` int(10) NOT NULL,
  `minute` int(10) NOT NULL,
  `second` int(10) NOT NULL,
  `check_value` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pill`
--

INSERT INTO `pill` (`id`, `name`, `quantity`, `box`, `day`, `month`, `year`, `hour`, `minute`, `second`, `check_value`, `datetime`) VALUES
(1, 'DO NOT REMOVE', 1, 0, 1, 1, 1, 1, 1, 1, 'off', '0000-00-00 00:00:00'),
(108, 'Ketonal', 1, 1, 26, 5, 2020, 12, 0, 1, 'off', '2020-05-26 12:00:01'),
(109, 'Lupocet', 1, 2, 27, 5, 2020, 12, 0, 1, 'off', '2020-05-27 12:00:01'),
(110, 'Claritin', 1, 3, 28, 5, 2020, 12, 0, 1, 'off', '2020-05-28 12:00:01'),
(111, 'Belodin', 1, 4, 29, 5, 2020, 12, 0, 1, 'off', '2020-05-29 12:00:01'),
(112, 'Ibufix', 1, 5, 30, 5, 2020, 12, 0, 1, 'off', '2020-05-30 12:00:01'),
(113, 'Ibuprofen', 1, 6, 31, 5, 2020, 12, 0, 1, 'off', '2020-05-31 12:00:01'),
(114, 'Nalgesin', 1, 7, 1, 6, 2020, 12, 0, 1, 'off', '2020-06-01 12:00:01'),
(115, 'Neofen', 1, 8, 2, 6, 2020, 12, 0, 1, 'off', '2020-06-02 12:00:01'),
(116, 'Lekadol', 1, 9, 3, 6, 2020, 12, 0, 1, 'off', '2020-06-03 12:00:01'),
(117, 'Ibuxin', 1, 10, 4, 6, 2020, 12, 0, 1, 'off', '2020-06-04 12:00:01'),
(118, 'Efferalgan', 1, 11, 5, 6, 2020, 12, 0, 1, 'off', '2020-06-05 12:00:01'),
(119, 'Panadon', 1, 12, 6, 6, 2020, 12, 0, 1, 'off', '2020-06-06 12:00:01'),
(120, 'Apaurin', 1, 13, 7, 6, 2020, 12, 0, 1, 'off', '2020-06-07 12:00:01'),
(121, 'Normabel', 1, 14, 8, 6, 2020, 12, 0, 1, 'off', '2020-06-08 12:00:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pill`
--
ALTER TABLE `pill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_box` (`box`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pill`
--
ALTER TABLE `pill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
