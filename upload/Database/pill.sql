-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 22, 2020 at 12:07 AM
-- Server version: 5.7.30-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pillorga_pill`
--

-- --------------------------------------------------------

--
-- Table structure for table `pill`
--

CREATE TABLE `pill` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(5) NOT NULL,
  `box` int(10) NOT NULL,
  `day` int(10) NOT NULL,
  `month` int(10) NOT NULL,
  `year` int(10) NOT NULL,
  `hour` int(10) NOT NULL,
  `minute` int(10) NOT NULL,
  `second` int(10) NOT NULL,
  `check_value` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pill`
--

INSERT INTO `pill` (`id`, `name`, `quantity`, `box`, `day`, `month`, `year`, `hour`, `minute`, `second`, `check_value`, `datetime`) VALUES
(1, 'DO NOT REMOVE', 1, 0, 1, 1, 1, 1, 1, 1, 'off', '0000-00-00 00:00:00'),
(66, 'Neofen', 1, 1, 20, 5, 2020, 15, 0, 0, 'on', '2020-05-20 15:00:00'),
(75, 'Pilex', 2, 2, 15, 5, 2021, 15, 15, 15, 'off', '2021-05-15 15:15:15'),
(76, 'Master cure', 1, 5, 12, 5, 2021, 12, 0, 0, 'off', '2021-05-12 12:00:00'),
(77, 'Ketonal', 1, 4, 22, 5, 2020, 13, 5, 1, 'off', '2020-05-22 13:05:01'),
(78, 'Trulex', 1, 6, 18, 5, 2020, 13, 1, 1, 'off', '2020-05-18 13:01:01'),
(79, 'Smrdex', 1, 8, 17, 5, 2020, 14, 1, 1, 'off', '2020-05-17 14:01:01'),
(80, 'Drmex', 1, 10, 15, 5, 2020, 16, 1, 1, 'off', '2020-05-15 16:01:01'),
(81, 'Domestos', 1, 12, 12, 5, 2020, 15, 5, 1, 'off', '2020-05-12 15:05:01'),
(85, 'Koljac', 1, 15, 12, 12, 2020, 1, 1, 1, 'off', '2020-12-12 01:01:01'),
(86, 'Prob1', 1, 3, 12, 12, 2020, 2, 2, 2, 'off', '2020-12-12 02:02:02'),
(87, 'sfdkdm', 12, 7, 5, 1, 2020, 1, 1, 1, 'off', '2020-01-05 01:01:01'),
(88, 'Proba9', 12, 9, 9, 1, 2020, 12, 12, 12, 'off', '2020-01-09 12:12:12'),
(89, 'Proba11', 12, 11, 11, 1, 2020, 1, 1, 1, 'off', '2020-01-11 01:01:01'),
(90, 'Proba13', 1, 13, 13, 1, 2020, 1, 1, 1, 'off', '2020-01-13 01:01:01'),
(91, 'Proba14', 1, 14, 14, 1, 2020, 1, 1, 1, 'off', '2020-01-14 01:01:01'),
(92, 'Proba16', 1, 16, 16, 1, 2020, 1, 1, 1, 'off', '2020-01-16 01:01:01'),
(93, 'Proba17', 2, 17, 17, 1, 2020, 1, 1, 1, 'off', '2020-01-17 01:01:01'),
(94, 'Proba18', 2, 18, 18, 1, 2020, 1, 1, 1, 'off', '2020-01-18 01:01:01'),
(95, 'Proba19', 2, 19, 19, 1, 2020, 1, 1, 1, 'off', '2020-01-19 01:01:01'),
(96, 'Proba20', 1, 20, 20, 1, 2020, 1, 1, 1, 'off', '2020-01-20 01:01:01'),
(97, 'Proba21', 1, 21, 21, 1, 2020, 1, 1, 1, 'off', '2020-01-21 01:01:01'),
(98, 'Proba22', 1, 22, 22, 1, 2020, 1, 1, 1, 'off', '2020-01-22 01:01:01'),
(99, 'Proba23', 1, 23, 23, 1, 2020, 1, 1, 1, 'off', '2020-01-23 01:01:01'),
(100, 'Proba24', 1, 24, 24, 1, 2020, 1, 1, 1, 'off', '2020-01-24 01:01:01'),
(101, 'Proba25', 1, 25, 25, 1, 2020, 1, 1, 1, 'off', '2020-01-25 01:01:01'),
(102, 'Proba26', 1, 26, 26, 1, 2020, 1, 1, 1, 'off', '2020-01-26 01:01:01'),
(103, 'Proba27', 1, 27, 27, 1, 2020, 1, 1, 1, 'off', '2020-01-27 01:01:01'),
(104, 'Proba28', 1, 28, 28, 1, 2020, 1, 1, 1, 'off', '2020-01-28 01:01:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pill`
--
ALTER TABLE `pill`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UC_box` (`box`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pill`
--
ALTER TABLE `pill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
